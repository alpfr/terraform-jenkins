variable "aws-instance-type" {
  description = "ec2 instance type"
  type        = string
  default     = "t2.micro"
}
variable "aws-instance-ami" {
  description = "ec2 instance ami"
  type        = string
  default     = "ami-00aa0673b34e3c150"
}
variable "aws-instance-key" {
  description = "ec2 instance key pair"
  type        = string
  default     = "awsusw2-devops-keypair.pem"
}
