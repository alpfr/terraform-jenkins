output "redhat2_public_ip" {
  description = "Public IP of jenkins instance 2"
  value       = aws_instance.jenkins.public_ip
}

output "redhat2_private_ip" {
  description = "Private IP of jenkins instance 2"
  value       = aws_instance.jenkins.private_ip
}

output "vpc_id" {
  description = "ID of the vpc"
  value       = aws_vpc.jenkins_vpc.id
}

output "instance_id" {
  description = "ID of EC2 instance"
  value       = aws_instance.jenkins.id
}
